<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Breeze Admin</title>
    <link rel="stylesheet" href="/breeze/template/assets/vendors/mdi/css/materialdesignicons.min.css" />
    <link rel="stylesheet" href="/breeze/template/assets/vendors/flag-icon-css/css/flag-icon.min.css" />
    <link rel="stylesheet" href="/breeze/template/assets/vendors/css/vendor.bundle.base.css" />
    <link rel="stylesheet" href="/breeze/template/assets/vendors/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/breeze/template/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css" />
    <link rel="stylesheet" href="/breeze/template/assets/css/style.css" />
    <link rel="shortcut icon" href="/breeze/template/assets/images/favicon.png" />
  </head>
  <body>
    <div class="container-scroller">
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <div class="text-center sidebar-brand-wrapper d-flex align-items-center">
          <a class="sidebar-brand brand-logo" href="index.html"><img src="/breeze/template/assets/images/logo.svg" alt="logo" /></a>
          <a class="sidebar-brand brand-logo-mini pl-4 pt-3" href="index.html"><img src="/breeze/template/assets/images/logo-mini.svg" alt="logo" /></a>
        </div>
        {{-- SIDEBAR --}}
        @include('partial.sidenav')
        {{-- NAVBAR --}}
        @include('partial.navbar')
        {{-- ISI --}}
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title">@yield('judul')</h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Forms</a></li>
                  <li class="breadcrumb-item active" aria-current="page"> Form elements </li>
                </ol>
              </nav>
            </div>
        @yield('content')
          <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
              <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © ResepKita.com 2021</span>
              <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> Free <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap dashboard template</a> from Bootstrapdash.com</span>
            </div>
          </footer>
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="/breeze/template/assets/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="/breeze/template/assets/vendors/chart.js/Chart.min.js"></script>
    <script src="/breeze/template/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="/breeze/template/assets/vendors/flot/jquery.flot.js"></script>
    <script src="/breeze/template/assets/vendors/flot/jquery.flot.resize.js"></script>
    <script src="/breeze/template/assets/vendors/flot/jquery.flot.categories.js"></script>
    <script src="/breeze/template/assets/vendors/flot/jquery.flot.fillbetween.js"></script>
    <script src="/breeze/template/assets/vendors/flot/jquery.flot.stack.js"></script>
    <script src="/breeze/template/assets/vendors/flot/jquery.flot.pie.js"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="/breeze/template/assets/js/off-canvas.js"></script>
    <script src="/breeze/template/assets/js/hoverable-collapse.js"></script>
    <script src="/breeze/template/assets/js/misc.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="/breeze/template/assets/js/dashboard.js"></script>
    <!-- End custom js for this page -->
  </body>
</html>