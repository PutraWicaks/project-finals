@extends('layout.master')

@section('judul')
<h1>Register Form</h1>
@endsection

@section('content')
                    <form action="{{ route('register') }}" method="POST" class="forms-sample">
                        @csrf
                      <div class="form-group">
                        <label for="exampleInputUsername1">Username</label>
                        <input type="text" name="username" class="form-control" id="" placeholder="Username" />
                      </div>
                      @error('username')
                      <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" name="email" class="form-control" id="" placeholder="Email" />
                      </div>
                      @error('email')
                      <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                      <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" name="password" class="form-control" id="" placeholder="Password" />
                      </div>
                      @error('password')
                      <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                      <div class="form-group">
                        <label for="exampleInputConfirmPassword1">Confirm Password</label>
                        <input type="password" name="password" class="form-control" id="" placeholder="Password" />
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Umur</label>
                        <input type="email" name="umur" class="form-control" id="" placeholder="Isi Umur" />
                      </div>
                      @error('umur')
                      <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                      <div class="form-group">
                        <label for="exampleInputEmail1">Biodata</label>
                        <textarea name="bio" class="form-control" id="" placeholder="Isi Biodata"></textarea>
                      </div>
                      @error('bio')
                      <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                      <div class="form-group">
                        <label for="exampleInputEmail1">Alamat</label>
                        <textarea name="alamat" class="form-control" id="" placeholder="Isi Alamat"></textarea>
                      </div>
                      @error('alamat')
                      <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                      <button type="submit" class="btn btn-primary mr-2"> Submit </button>
                      <button class="btn btn-light">Cancel</button>
                    </form>
                  </div>
                </div>
          </div>
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <script src="/breeze/template/assets/vendors/js/vendor.bundle.base.js"></script>
    <script src="/breeze/template/assets/vendors/select2/select2.min.js"></script>
    <script src="/breeze/template/assets/vendors/typeahead.js/typeahead.bundle.min.js"></script>
    <script src="/breeze/template/assets/js/off-canvas.js"></script>
    <script src="/breeze/template/assets/js/hoverable-collapse.js"></script>
    <script src="/breeze/template/assets/js/misc.js"></script>
    <script src="/breeze/template/assets/js/file-upload.js"></script>
    <script src="/breeze/template/assets/js/typeahead.js"></script>
    <script src="/breeze/template/assets/js/select2.js"></script>
@endsection