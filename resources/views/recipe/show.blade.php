@extends('layout.master')

@section('judul')
<h1>Detail Resep {{$film->judul}}</h1>
@endsection

@section('content')

<img src="{{asset('thumbImg/'.$recipe->thumbnail)}}" alt="">
<h1>{{$recipe->judul}}</h1>
<p>{{$recipe->content}}</p>

<h1>Komentar</h1>
@foreach ($recipe->komentar as $item)
    <div class="card" style="width: 18rem;">
        <div class="card-body">
          <h5><b>{{$item->user->name}}</b></h5>
          <p class="card-text">{{$item->isi}}</p>
        </div>
      </div>
    </div>
@endforeach

<form action="/komentar" method="POST" enctype="multipart/form-data">
    @csrf 
      <div class="form-group">
          <label>Komentar/Saran</label>
          <input type="hidden" name="user_id" value="{{$film->id}}" class="form-control">
          <textarea name="komentar" class="form-control" id="" cols="30" rows="10"></textarea>
        </div>
      @error('komentar')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
<a href="/recipe" class="btn btn-success">Kembali</a>

@endsection