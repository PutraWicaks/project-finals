@extends('layout.master')

@section('judul')
<h1>Edit Resep</h1>
@endsection

@section('content')
<form action="/recipe/{{$recipe->id}}" value="{{$recipe->judul}}" method="POST" enctype="multipart/form-data">
  @csrf
  @method('PUT')
    <div class="form-group">
      <label>Judul</label>
      <input type="text" name="judul" class="form-control">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Content</label>
        <textarea name="content" class="form-control" id="" cols="30" rows="10">{{$recipe->content}}</textarea>
      </div>
    @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Kategori</label>
        <select name="kategori_id" class="form-control" id=""></select>
        @foreach ($recipe as $item)
        <option value="">---Pilih genre---</option>
        @if ($item->id === $recipe->kategori_id)
        
        <option value="{{$item->id}}" selected>{{$item->nama}}</option>
        @else
        <option value="{{$item->id}}">{{$item->nama}}</option>
        @endif
        @endforeach
    </div>
    @error('kategori_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Thumbnail</label>
        <input type="file" name="thumbnail" class="form-control">
      </div>
      @error('thumbnail')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection