@extends('layout.master')

@section('judul')
<h1>Index Recipe</h1>
@endsection

@section('content')
@auth
<a href="/recipe/create"class="btn btn-primary my-2">Add</a>
@endauth
<div class="row">
    @forelse ($recipe as $item)
    <div class="col-4">
    <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="{{asset('thumbImg/'. $item->thumbnail)}}" alt="...">
        <div class="card-body">

          <h5>{{$item->judul}}</h5>
          <p class="card-text">{{$item->content}}</p>
          @auth
          <form action="/recipe/{{$item->id}}"method="POST"></form>
          @csrf
          @method('DELETE')
          <a href="/recipe/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
          <a href="/recipe/{{$item->id}}/edit" class="btn btn-secondary btn-sm">Edit</a>
          <input type="submit" value="Delete" class="btn btn-Danger btn-sm">Delete>
          @endauth
          @guest
          <a href="/recipe/{{$recipe->id}}" class="btn btn-primary btn-sm">Detail</a>
          @endguest
        </div>
      </div>
    </div>
        
    @empty
        <h3>Data Film Kosong</h3>
    @endforelse
</div>

@endsection