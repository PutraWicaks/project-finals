@extends('layout.master')

@section('judul')
<h1>Tambah Resep</h1>
@endsection

@section('content')
<form action="/recipe" method="POST" enctype="multipart/form-data">
  @csrf 
    <div class="form-group">
      <label>Judul</label>
      <input type="text" name="judul" class="form-control">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Content</label>
        <textarea name="content" class="form-control" id="" cols="30" rows="10"></textarea>
      </div>
    @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
     <div class="form-group">
      <label>Kategori</label>
      <select name="kategori_id" class="form-control" id="">
      <option value="">---Pilih genre---</option>
      @foreach ($kategori as $item)
      <option value="{{$item->id}}">{{$item->nama}}</option>
      @endforeach
    </select>
    </div>
    @error('kategori_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Thumbnail</label>
        <input type="file" name="thumbnail" class="form-control">
      </div>
      @error('thumbnail')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection