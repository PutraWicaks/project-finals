@extends('layout.master')

@section('judul')
<h1>Form Kategori</h1>
@endsection

@section('content')
<form action="/kategori" method="POST">
    @csrf 
      <div class="form-group">
        <label>Nama Kategori</label>
        <input type="text" name="nama" class="form-control">
      </div>
      @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
          <label>Deskripsi</label>
          <textarea name="deskripsi" class="form-control" id="" cols="30" rows="10"></textarea>
        </div>
      @error('deskripsi')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection