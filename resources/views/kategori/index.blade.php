@extends('layout.master')

@section('judul')
<h1>Index Kategori</h1>
@endsection

@section('content')
<a href="/kategori/create" class="btn btn-secondary mb-2">Tambah Kategori</a>
<table class="table table-striped table-dark">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Deskripsi</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
    @forelse ($kategori as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->deskripsi}}</td>
        </td>
            <td>
                <form action="/kategori/{{$item->id}}" method="POST">
                    <a href="/kategori/{{$item->id}}" class="btn btn-primary btn-sm">Show</a>
                    <a href="/kategori/{{$item->id}}/edit" class="btn btn-info btn-sm">edit</a>
                    @method('delete')
                    @csrf

                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>
        </tr>
    @empty
        <tr>
            <td>-</td>
        </tr>
    @endforelse
    </tbody>
  </table>
@endsection