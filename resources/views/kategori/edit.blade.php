@extends('layout.master')

@section('judul')
<h1>Edit Form Kategori{{$kategori->nama}}</h1>
@endsection

@section('content')
<form action="/kategori/{{$kategori->id}}" method="POST">
  @csrf
  @method('PUT')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" value="{{$kategori->nama}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Deskripsi</label>
        <textarea name="deskripsi" class="form-control" id="" cols="30" rows="10">{{$kategori->deskripsi}}</textarea>
      </div>
    @error('deskripsi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection