<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Recipe;
use File;

class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recipe = Recipe::all();
        return view('recipe.index',compact('recipe'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = DB::table('kategori')->get();
        return view('recipe.create',compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request ->validate([
            'judul' => 'required',
            'content' => 'required',
            'kategori_id' => 'required',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $thumbnailNama = time().'.'.$request->thumbnail->extension();

        $request->thumbnail->move(public_path('thumbImg'),$thumbnailNama);

        $recipe = new Recipe;

        $recipe->judul = $request->judul;
        $recipe->content = $request->content;
        $recipe->kategori_id = $request->kategori_id;
        $recipe->thumbnail = $thumbnailNama;

        $recipe->save();

        return redirect('/recipe/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recipe = Recipe::findOrFail($id);
        return view('recipe.show',compact('recipe'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = DB::table('kategori')->get();
        $recipe = Recipe::findOrFail($id);
        return view('recipe.edit',compact('recipe','kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request ->validate([
            'judul' => 'required',
            'content' => 'required',
            'kategori_id' => 'required',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $recipe = Recipe::find($id);
        if($request->has('thumbnail')){
            $thumbnailNama = time().'.'.$request->thumbnail->extension();

            $request->thumbnail->move(public_path('thumbImg'),$thumbnailNama);

            $recipe->judul = $request->judul;
            $recipe->content = $request->content;
            $recipe->kategori_id = $request->kategori_id;
            $recipe->thumbnail = $thumbnailNama;
        }else{
            $recipe->judul = $request->judul;
            $recipe->content = $request->content;
            $recipe->kategori_id = $request->kategori_id;
        }

        $recipe->update();
        return redirect('/recipe');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $recipe = Recipe::find($id);
        $path = "thumbImg/";
        File::delete($path. $recipe->thumbnail);
        $recipe->delete();    
    
        return redirect('/recipe');
    }
}
